@extends('layout')

@section('body')

@php $user = auth()->user(); @endphp

<div class="container-fluid">
<div class="row justify-content-center" style="margin-top: 2%">
<div class="col-xl-6">

    <h3 class="text-center mt-3"> Profile </h3> <hr>
    <div class="w-100 d-flex justify-content-center">
    <div style="max-width: 400px">
        <form action="/edit-profile" method="POST"> @csrf
            <label> Όνομα </label>
            <input type="text" name="name" value="{{ $user->name }}" placeholder="username" class="form-control my-2">

            <label> Email </label>
            <input type="email" name="email" value="{{ $user->email }}" placeholder="email" class="form-control my-2">
            @if( $errors->has('email') ) <span class="text-danger"> {{ $errors->first('email') }} </span> @endif

            <label> Κωδικός </label>
            <input type="password" name="password" placeholder="Συμπληρώνεται μονο για αλλαγή" class="form-control my-2">
            <label> Επιβεβαίωση Κωδικού </label>
            <input type="password" name="password_confirmation" placeholder="Συμπληρώνεται μονο για αλλαγή" class="form-control my-2">
            @if( $errors->has('password') ) <span class="text-danger"> {{ $errors->first('password') }} </span> @endif
            @if( $errors->has('password_confirmation') ) <span class="text-danger"> {{ $errors->first('password_confirmation') }} </span> @endif

            <div class="form-check my-3">
                <label class="form-check-label"> I accept <a href="/terms"> Terms </a> and conditions to share my data </label>
                <input type="checkbox" name="terms" class="form-check-input" checked disabled>
            </div>

            <div class="form-check my-3">
                <label class="form-check-label"> I accept to share my data and get personalized data </label>
                <input type="checkbox" name="allowed_data_share" class="form-check-input" @if( $user->allowed_data_share ) checked @endif>
            </div>

            <div class="d-flex justify-content-center">
                <input type="submit" class="btn btn-outline-primary my-2" value="Update Info">
            </div>

            @if( session()->has('editmess') ) <span class="text-success"> {{ session()->get('editmess') }} </span> @endif
        </form>
    </div>
    </div>

    {{-- ------------------------------------------------------------------------------------------------------------------------------- --}}
    {{-- ------------------------------------------------------------------------------------------------------------------------------- --}}

    <h3 class="text-center mb-3" style="margin-top:4rem;"> Connect my Paypal account </h3>
    <hr>
    <div class="d-flex justify-content-center">
        <div> <img src="/images/paypal_run247_userstats.png" alt=""> </div>
    </div>
    <div class="d-flex justify-content-center mt-4">
        <div> <button class="btn btn-outline-primary disabled"> Connect </button> </div>
    </div>
    <div class="mb-5"></div>

</div>
</div>
</div>

@endsection