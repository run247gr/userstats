@extends('layout')

@section('body')

<div class="row mt-3"> 
    <h1 class="text-center"> Charts </h1>
    <hr>
</div>

<div class="row pt-5" > {{-- style="height:780px;  overflow-y: scroll;" --}}
    @foreach( App\Models\Userstat::chartLabels() as $field => $label )
    @if($field != "tracker_type" )
    <div class="col-md-6" >
        @include('fieldchart',["field" => $field,])
    </div>
    @endif
    @endforeach
</div>

@endsection