@extends('layout')

@section('body')

@php $labels = App\Models\Userstat::tableLabels(); 
    $stats = App\Models\Userstat::where('user_id',Illuminate\Support\Facades\Auth::user()->id)->get(); // ->sortByDesc('activity_date')
@endphp
<div class="container-fluid">

    <div class="my-5"> <h4 class="userstat-title"> Your Activities</h4> </div>

    <div> {{-- style="overflow: scroll; height:80vh" --}}
        <table id="activities-table" class="table table-hover" style="font-size: 14px;">
            <thead>
            <tr>
                {{-- <th> ID </th> --}}
                @foreach( $labels as $label )
                <th> {{ $label }} </th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach( $stats as $stat )
            <tr>
                {{-- <td> {{ $stat->id }} </td> --}}
                @foreach( $labels as $field => $label  )
                <td> {{ $stat->$field }} </td>
                @endforeach
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>

<script>
$(document).ready(function () {
    $('#activities-table').DataTable({ scrollX: true, order: [[0, 'desc']], pageLength: 50 });
});
</script>

@endsection