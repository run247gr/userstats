@extends('layout')

@section('body')

<div class="d-flex justify-content-center my-5">
    <h1> Welcome </h1>
</div>
<div class="d-flex justify-content-center my-5">
    <img src="/images/profibit_logo2.png" alt="profibit_logo" class="img" style="width:16vw; height: auto;">
</div>

<div class="row mb-5" style="height:800px">
    
    <div class="col-md-8 offset-2">

        @include('home.carousel')
    
    </div>

</div>

@endsection