@php

    $pics = [
        "https://run247.gr/sites/default/files/slider_3.jpg",
        "https://run247.gr/sites/default/files/slider_1.jpg",
        "https://run247.gr/sites/default/files/slider_2.jpg",
    ];

@endphp


<div id="picCarousel" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        @foreach( $pics as $i => $pic )
        
        <button type="button" data-bs-target="#picCarousel" data-bs-slide-to="{{$i}}" @if($i==0) class="active" @endif aria-label="Slide 1"></button>

        @endforeach
    </div>
    <div class="carousel-inner">

        @foreach ( $pics as $i => $pic )
        <div class="carousel-item @if($i==0) active @endif">
            <img src="{{$pic}}" class="d-block w-100 rounded"   style="max-height: 70vh;"  alt="pic1">
            {{-- 
                <div class="carousel-caption d-none d-md-block">
                    <h5>Pic1 Title</h5>
                    <p>Pic1 Caption.</p>
                </div>
            --}}
        </div>
        @endforeach
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#picCarousel" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#picCarousel" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>