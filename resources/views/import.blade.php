@extends('layout')

@section('body')

@php
    $user = \Illuminate\Support\Facades\Auth::user();
@endphp

<div class="container-fluid my-5">

    <div class="row justify-content-around">
        <div class="col-lg-4">
            <div class="card shadow">
                <div class="card-body">
                    <h4 class="mb-3"> Garmin Import - From File </h4>
                    <form action="/garmin-import" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-9"> 
                                <input type="file" name="file_garmin" class="form-control"> 
                                @if( $errors->has('file_garmin') ) <span class="text-danger"> {{ $errors->first('file_garmin') }} </span> 
                                @elseif( session()->has('file_garmin') ) <span class="text-success"> {{ session()->get('file_garmin') }} </span> 
                                @else <span> &nbsp; </span> @endif
                            </div>
                            <div class="col-lg-3"> <button class="btn btn-primary" type="submit"> Submit </button> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card shadow">
                <div class="card-body">
                    <h4 class="mb-3"> Strava Import - From File </h4>
                    <form action="/strava-import" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-9"> 
                                <input type="file" name="file_strava" class="form-control"> 
                                @if( $errors->has('file_strava') ) <span class="text-danger"> {{ $errors->first('file_strava') }} </span> 
                                @elseif( session()->has('file_strava') ) <span class="text-success"> {{ session()->get('file_strava') }} </span> 
                                @else <span> &nbsp;</span> @endif
                            </div> 
                            <div class="col-lg-3"> <button class="btn btn-primary" type="submit"> Submit </button> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card shadow">
                <div class="card-body">
                    <h4 class="mb-3"> Strava Import - From App </h4>
                    <div class="row">
                    @if( $user->stravaExpired() )
                        <div class="col-lg-8">  
                            <div class="input-group">
                                <span class="input-group-text"> Authentication with strava app required </span>
                            </div>
                            @if( $errors->has('app_strava') ) <span class="text-danger"> {{ $errors->first('app_strava') }} </span> 
                            @elseif( session()->has('app_strava') ) <span class="text-success"> {{ session()->get('app_strava') }} </span> 
                            @else <span> &nbsp; </span> @endif
                        </div>
                        <div class="col-lg-4"> 
                            <a href="/api/strava-getaccess" class="btn btn-primary"> Authenticate </a> 
                        </div>
                    @else
                        <div class="col-lg-9">  
                            <div class="input-group">
                                <span class="input-group-text"> Import Your Athlete Activities </span>
                            </div>
                            @if( $errors->has('ath_strava') ) <span class="text-danger"> {{ $errors->first('ath_strava') }} </span> 
                            @elseif( session()->has('ath_strava') ) <span class="text-success"> {{ session()->get('ath_strava') }} </span> 
                            @else <span> &nbsp; </span> @endif
                        </div>
                        <div class="col-lg-3"> 
                            <form action="/strapp-import" method="POST">
                                @csrf
                                <div> <button class="btn btn-primary" type="submit"> Import </button> </div>
                                @if( $user->isadmin )
                                    <a href="/api/strava-getaccess"> Re-Authenticate </a>
                                @endif
                            </form>
                        </div>
                    @endif
                    </div>      
                </div>
            </div>
        </div>
       
    </div>
</div>

<div class="container-fluid my-5">

    <div class="d-flex flex-row justify-content-center">
        <div style="margin-right:10px;"> <h5 style="padding-top:30px;"> Use Profibit services to import data from your favourite tracking app </h5> </div>
        <div> <img src="/images/profibit_logo2.png" alt="profibit_logo" class="img" style="width:16vw; height: auto"> </div>
        <div> <img src="/images/dapsi_logo.png"     alt="dapsi_logo"    class="img" style="width:16vw; height: auto"> </div>
    </div>

</div>

<hr>

@if( $user->isadmin )
    <div class="input-group">
        <span class="input-group-text"> Bearer Token </span>
        <span class="input-group-text"> {{ $user->strava_access }} </span>
    </div>
    @if( session()->has('res_obj') && !session()->has('stats') )
        <pre style="font-size: 12px"> {!! json_encode(session()->get('res_obj'),JSON_PRETTY_PRINT ) !!} </pre>
    @endif
@endif

@if( session()->has('stats') )

    @php
        $stats  = session()->get('stats');
        $labels = App\Models\Userstat::LABELS; 
        $fields = App\Models\Userstat::FIELDS;
    @endphp
    <div class="container-fluid">
        <h4> Newly Imported Data : {{ count($stats) }} </h4>
        @if( count($stats) > 0 )
            <div style="overflow: scroll; height:80vh">
                <table class="table table-striped table-hover" style="font-size: 12px;">
                    <thead>
                    <tr>
                        {{-- <th> ID </th> --}}
                        @foreach( $labels as $key => $label )
                        @if( $key != 'user_id' )
                        <th> {{ $label }} </th>
                        @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( session()->get('stats') as $stat )
                    <tr>
                        {{-- <td> {{ $stat->id }} </td> --}}
                        @foreach( $fields as $field )
                        @if( $field != 'user_id' )
                        <td> {{ $stat->$field }} </td>
                        @endif
                        @endforeach
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <p class="mb-2"> (none) </p>
        @endif      
    </div>
@endif


@endsection