@extends('layout')

@section('body')


<div class="container my-5">

    <div class="row justify-content-center">
        <div class="col-md-6">

            <div class="card shadow mt-5">
                <div class="card-header">
                    <h2 class="text-center">  Strava App Config </h2>
                </div>
                <div class="card-body">

                    <form action="/admin/config" method="POST">
                        @csrf
                        <div class="mt-4">
                            <label for="client_id" class="form-label">Client ID</label>
                            <input type="text" id="client_id" name="client_id" class="form-control" value="{{ $client_id }}">
                        </div>
                
                        <div class="mt-4">
                            <label for="client_secret" class="form-label">Client Secret</label>
                            <input type="text" id="client_secret" name="client_secret" class="form-control" value="{{ $client_secret }}">
                        </div>

                        <div class="mt-4">
                            <label for="redirect_uri" class="form-label"> Redirect Uri</label>
                            <input type="text" id="redirect_uri" name="redirect_uri" class="form-control" value="{{ $redirect_uri }}">
                        </div>
                
                
                        {{-- <div class="mt-4">
                            <label for="access_token" class="form-label">Access  Token</label>
                            <input type="text" id="access_token" name="access_token" class="form-control" value="{{ $access_token }}">
                        </div>
                
                        <div class="mt-4">
                            <label for="refresh_token" class="form-label">Refresh Token</label>
                            <input type="text" id="refresh_token" name="refresh_token" class="form-control" value="{{ $refresh_token }}">
                        </div> --}}

                        <div class="mt-4 d-flex justify-content-center">
                            <input type="submit" type="button" class="btn btn-primary" value="Save">
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>

    <div class="row justify-content-center">
        <div class="mt-4">
            @if( session()->has('config_data') ) 

                <h3 class="text-success text-center" > {{ session()->get('config_data') }} </h3>
            @endif
        </div>
    </div>


</div>

@endsection