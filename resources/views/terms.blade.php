@extends('layout')

@section('body')

<div class="container">
    <div class="row" style="margin-top: 8%">
        <div class="col-md-4 offset-md-4">
            <div class="card shadow">
                <div class="card-body">
                    <img src="/images/run_247_logo.png" alt="run_247_logo" class="img w-100 my-2">
    
                    <h3 class="text-center mt-3"> Terms of Use </h3>
                    <hr>
                    <div style="height: 400px; overflow-y:scroll">
    
                        @for( $i=0; $i<10; $i++)
                        <p>
                            There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, 
                            or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't 
                            anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, 
                            making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence 
                            structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, 
                            or non-characteristic words etc.
                        </p>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="card shadow mt-2">
                <div class="card-body">
                    <div class="d-flex justify-content-center">
                        @if( \Illuminate\Support\Facades\Auth::check() )
                        <a href="/profile" type="button" class="btn btn-outline-primary">
                            Profile
                        </a>
                        @else
                        <a href="/register" type="button" class="btn btn-outline-primary">
                            Register
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection