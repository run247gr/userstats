@extends('layout')

@section('body')

<div class="row py-5">
    <div class="col-md-6">
        <div>
            <h3>TEST import</h3>
            <form action="api/test-import" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col">
                        <input type="file" name="file" class="form-control">
                    </div>
                    <div class="col">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
                <div class="row">
                    <h4>
                        {{ $testMess ?? ''  }}
                    </h4>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row pb-5">

    @php use App\Models\Userstat; @endphp
    <div style="overflow: scroll; height:640px;">
        <table class="table table-striped table-hover" style="font-size: 12px;">
            <thead>
                <tr>
                    <th> ID </th>
                    @foreach( Userstat::LABELS as $label )
                    <th> {{ $label }} </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach( Userstat::all() as $stat )
                <tr>
                    <td> {{ $stat->id }} </td>
                    @foreach( Userstat::FIELDS as $field )
                    <td> {{ $stat->$field }} </td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection