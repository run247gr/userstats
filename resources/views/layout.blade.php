<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Run247 Userstats</title>
    <link rel="shortcut icon" href="https://run247.gr/sites/default/files/favicon.png" type="image/png">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.8.0/chart.min.js" 
    integrity="sha512-sW/w8s4RWTdFFSduOTGtk4isV1+190E/GghVffMA9XczdJ2MDzSzLEubKAs5h0wzgSJOQTRYyaz73L3d6RtJSg==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.js"></script>

    <link rel="stylesheet" href="css/style.css?v={{time()}}">
</head>

<body class="bg-light">
<nav class="navbar navbar-expand-lg shadow" style="background-color: white">
    <div class="container-fluid">
        <a class="navbar-brand border-end border-3" href="/">
            <img src="/images/run_247_logo.png" alt="" width="100" height="24" class="d-inline-block align-text-top">
            <span class="d-inline-block me-3"> Userstats </span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse flex justify-between" id="navCollapse">

            @if( \Illuminate\Support\Facades\Auth::check() ) {{-- ---------------------------------------------------------- --}}
            
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item me-2"> <a class="nav-link" href="/import" > Import </a> </li>
                <li class="nav-item me-2"> <a class="nav-link" href="/export" > Export </a> </li>
                <li class="nav-item me-2"> <a class="nav-link" href="/activities" > My Activities  </a> </li>
                <li class="nav-item me-2"> <a class="nav-link" href="/chart"  > Charts  </a> </li>
                <li class="nav-item me-2"> <a class="nav-link" href="/howto"  > How to  </a> </li>
                <li class="nav-item me-2"> <a class="nav-link" href="/api/documentation"  > Api Doc  </a> </li>
                
                @if( \Illuminate\Support\Facades\Auth::user()->isadmin )
                <li class="nav-item border-end border-3"> </li>
                <li class="nav-item me-2"> <a class="nav-link" href="/clear-data"  > Clear Data    </a> </li>
                <li class="nav-item me-2"> <a class="nav-link" href="/gencsv"      > Generate Data </a> </li>
                <li class="nav-item me-2"> <a class="nav-link" href="/admin/config"> Strava Config </a> </li>
                @endif
            </ul>

            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown me-7">
                    <a class="nav-link dropdown-toggle fw-bold" href="/profile" id="navbarDropdown" role="button" 
                        data-bs-toggle="dropdown" aria-expanded="false">
                        {{ \Illuminate\Support\Facades\Auth::user()->name }}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li class="nav-item">
                            <a class="dropdown-item" href="/profile"> Profile </a>
                        </li>
                        <li class="nav-item">
                            <form action="/logout" method="POST"> @csrf
                                <input type="submit" class="logout-input dropdown-item" value="Log Out">
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>

            @else {{-- ----------------------------------------------------------------------------------------------------- --}}
            
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/register">Register</a>
                </li>
            </ul>

            @endif {{-- ---------------------------------------------------------------------------------------------------- --}}

        </div>
    </div>
</nav>

<div class="container-fluid"> @yield('body') </div>
    
</body>
</html>