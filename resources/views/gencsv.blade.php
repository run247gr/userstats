@extends('layout')

@section('body')

<div class="container-fluid">
    <div class="row my-5">

        <div class="col-md-4 offset-md-4">

            <div class="card shadow mt-5">
                <div class="card-header"> Generate Data </div>
                <div class="card-body">
                    
                    <form action="/generate-data" method="POST">
                        @csrf
                        <div class="d-flex justify-content-around">
                            <div class="me-5">
                                <label for="number" class="form-label">Data Entries</label>
                                <input id="number" type="number" name="number" class="form-control" required>
                                @if( $errors->has('number') ) <span class="text-danger">{{ $errors->first('number') }} </span> @endif
                            </div>
                            <div>
                                <label for="user_id" class="form-label">User id</label>
                                <input id="user_id" type="number" name="user_id" class="form-control" required>
                                @if( $errors->has('user_id') ) <span class="text-danger">{{ $errors->first('user_id') }} </span> @endif
                            </div>
                        </div>
                        <div class="mt-5 d-flex ">
                            <div class="ms-4">
                                <input type="submit" class="btn btn-outline-primary" value="Generate">
                            </div>
                        </div>
                        @if( session()->has('gen_data') ) <span class="text-success"> {{ session()->get('gen_data') }} </span> @endif
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection