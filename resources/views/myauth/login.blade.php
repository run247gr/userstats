@extends('layout')

@section('body')

<div class="container">
    <div class="row" style="margin-top: 10%">
        <div class="col-md-4 offset-md-4">
            <div class="card shadow">
                <div class="card-body">
                    <img src="/images/run_247_logo.png" alt="run_247_logo" class="img w-100 my-2">

                    <h3 class="text-center mt-3"> Log In </h3>
                    <hr>

                    <form action="/login" method="POST">
                        @csrf
                        <input id=email      
                            type="email"    
                            name="email"    
                            value="{{ old('email') }}" 
                            placeholder="email"    
                            class="form-control my-2" 
                            required
                        >
                        <input id="password" type="password" class="form-control my-2" name="password" placeholder="password" required>

                        @if( $errors->has('email') ) <span class="text-danger">{{ $errors->first('email') }}</span> @endif
                        <div class="d-flex justify-content-center">
                            <input id="submit" type="submit" class="btn btn-outline-primary my-2" value="Log In">
                        </div>
                    </form>
                    <div class="d-flex justify-content-center mt-2">
                        <a href="/forgot-password" class="link-primary mt-5">I forgot my password</a>
                    </div>
                    <div class="d-flex justify-content-center mt-2">
                        <a href="/register" class="link-primary">Register</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection