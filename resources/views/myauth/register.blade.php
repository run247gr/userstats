@extends('layout')

@section('body')

<div class="container">
    <div class="row" style="margin-top: 10%">
        <div class="col-md-4 offset-md-4">
            <div class="card shadow">
                <div class="card-body">
                    <img src="/images/run_247_logo.png" alt="run_247_logo" class="img w-100 my-2">

                    <h3 class="text-center mt-3"> Register </h3>
                    <hr>

                    <form action="/register" method="POST">
                        @csrf
                        <input id=name       
                            type="text"     
                            name="name"     
                            value="{{ old('name') }}" 
                            placeholder="username" 
                            class="form-control my-2"
                            required
                        >

                        <input id=email      
                            type="email"    
                            name="email"    
                            value="{{ old('email') }}" 
                            placeholder="email"    
                            class="form-control my-2" 
                            required
                        >
                        @if( $errors->has('email') ) <span class="text-danger">{{ $errors->first('email') }}</span> @endif

                        <input id="password" 
                            type="password" 
                            name="password" 
                            placeholder="password" 
                            class="form-control my-2" 
                            required
                        >
                        <input id="password_confirmation" 
                            type="password" 
                            name="password_confirmation" 
                            placeholder="password confirmation" 
                            class="form-control my-2" 
                            required
                        >
                        @if( $errors->has('password') ) <span class="text-danger">{{ $errors->first('password') }}</span> @endif
                        @if( $errors->has('password_confirmation') ) <span class="text-danger">{{ $errors->first('password_confirmation') }}</span> @endif

                        <div class="form-check my-3">
                            <label for="terms" class="form-check-label"> 
                                I accept <a href="/terms"> Terms </a> and conditions to share my data
                            </label>
                            <input id="accepted_terms" 
                                type="checkbox" 
                                name="accepted_terms" 
                                class="form-check-input" 
                                required
                            >
                        </div>
                        @if( $errors->has('accepted_terms') ) <span class="text-danger">{{ $errors->first('accepted_terms') }}</span> @endif

                        <div class="form-check my-3">
                            <label for="terms" class="form-check-label"> 
                                I accept to share my data and get personalized data
                            </label>
                            <input id="allowed_data_share" 
                                type="checkbox" 
                                name="allowed_data_share" 
                                class="form-check-input"
                            >
                        </div>
                        @if( $errors->has('allowed_data_share') ) <span class="text-danger">{{ $errors->first('allowed_data_share') }}</span> @endif

                        <div class="d-flex justify-content-center">
                            <input id="submit" type="submit" class="btn btn-outline-primary my-2" value="Register">
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection