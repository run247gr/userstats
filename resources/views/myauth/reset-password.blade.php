
@extends('layout')

@section('body')

<div class="container">
    <div class="row" style="margin-top: 10%">
            
            <div class="col-md-4 offset-md-4">
                <div class="card shadow">
                    <div class="card-body">
                        <img src="/images/run_247_logo.png" alt="run_247_logo" class="img w-100 my-2">

                        <h3 class="text-center mt-3"> Password Recovery </h3>
                        <hr>

                        <form action="/reset-password" method="POST">
                            @csrf
                            <input type="hidden" name="token" value="{{ $request->route('token') }}">
                            <input id=email      
                                type="email"    
                                name="email"    
                                value="{{ $request->email }}" 
                                placeholder="email"    
                                class="form-control my-2" 
                                required
                            >
                            <input id="password" 
                                type="password" 
                                name="password" 
                                placeholder="password" 
                                class="form-control my-2" 
                                required
                            >
                            <input id="password_confirmation" 
                                type="password" 
                                name="password_confirmation" 
                                placeholder="password confirmation" 
                                class="form-control my-2" 
                                required
                            >
                            @if( $errors->has('token') ) <span class="text-danger"> {{ $errors->first('token') }} </span> @endif
                            @if( $errors->has('email') ) <span class="text-danger"> {{ $errors->first('email') }} </span> @endif
                            @if( $errors->has('password') ) <span class="text-danger"> {{ $errors->first('password') }} </span> @endif
                            @if( $errors->has('password_confirmation') ) <span class="text-danger"> {{ $errors->first('password_confirmation') }} </span> @endif
                            
                            <div class="d-flex justify-content-center">
                                <input id="submit" type="submit" class="btn btn-outline-primary my-2" value="Reset Password">
                            </div>
                        </form>

                    </div>
                </div>
            </div>

    </div>
</div>

@endsection