
@extends('layout')

@section('body')

<div class="container">
    <div class="row" style="margin-top: 10%">
            
            <div class="col-md-4 offset-md-4">
                <div class="card shadow">
                    <div class="card-body">
                        <img src="/images/run_247_logo.png" alt="run_247_logo" class="img w-100 my-2">

                        <h3 class="text-center mt-3"> Password Recovery </h3>
                        <hr>

                        <form action="/forgot-password" method="POST">
                            @csrf
                            <input id=email      
                                type="email"    
                                name="email"    
                                value="{{ old('email') }}" 
                                placeholder="email"    
                                class="form-control my-2" 
                                required
                            >
                            @if( $errors->has('email') ) <span class="text-danger">{{ $errors->first('email') }}</span> @endif
                            
                            <div class="d-flex justify-content-center">
                                <input id="submit" type="submit" class="btn btn-outline-primary my-2" value="Send Password Reset Mail">
                            </div>
                        </form>

                    </div>
                </div>
            </div>

    </div>
</div>

@endsection