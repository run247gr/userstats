@extends('layout')

@section('body')

<div class="container">
    <div class="p-5">
        <form action="/api/clear-data" method="POST">
            <button class="btn btn-danger" type="submit"> Clear Data
            </button>
        </form>
            
    </div>
</div>

@endsection