@extends('layout')

@section('body')

<div class="container-fluid my-5">

    <div class="row" style="margin-top:5%">
        <div class="col-md-4 offset-md-4">
                <div class="card shadow">
                    <div class="card-body">
    
                        <div class="row">
    
                            <h4 class="mb-5 text-center"> Export Your Data </h4>
    
                            <form action="export-data" method="POST">
                                @csrf
                                <div class="d-flex justify-content-center">
    
                                    <div style="margin-right:15px">
                                        <h5 class="pt-1"> Format Type : </h5>
                                    </div>
                                    <div style="margin-right:15px">
    
                                        <select name="format" id="format" class="form-select">
                                            <option value="csv"> CSV </option>
                                            <option value="xml"> XML </option>
                                            <option value="rdf"> RDF </option>
                                            <option value="json">JSON</option>
                                        </select>
                                    </div>
                                    <input type="submit" type="button" class="btn btn-primary" value="Export">
                                </div>
    
                            </form>
                        </div>
    
                    </div>
                </div>
            </div>
    </div>
</div>


@endsection