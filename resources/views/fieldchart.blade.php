@php  $data = App\Models\Userstat::fieldChartData($field); @endphp
<div class="mb-5">
    <h4 class="text-center"> {{ $label }} </h4>
    <div class="card shadow" style="border-radius:2rem;">
        <div class="card-body">
            <canvas id="chart_{{$field}}" style="hieght:300px !importa" ></canvas>
            {{-- <pre> {{ json_encode($data)}}</pre> --}}
        </div>
    </div>
</div>
<script>
    var labels = {!! json_encode($data["labels"]) !!};

    var data = {
        labels: labels,
        datasets: [{
            label: '{{ $label }}',
            @if( $data["chart_type"] == 'doughnut' )
                backgroundColor: [
                    'rgb(255, 99, 132)',
                    'rgb(54, 162, 235)',
                ],
            @else
                backgroundColor: 'rgb(34,139,34)',
            @endif
            borderColor: 'rgb(34,139,34)',
            data: {!! json_encode($data["data"]) !!},
        }],
    };

    var config = { type: '{{ $data["chart_type"] }}', data: data,
        // options: { responsive:true,maintainAspectRatio: false, }
    };

    var chart_{{$field}} = new Chart( document.getElementById('chart_{{$field}}'), config );
 
</script>
