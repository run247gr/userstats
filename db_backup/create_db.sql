CREATE USER 'run247'@'localhost' IDENTIFIED WITH mysql_native_password BY 'run247pass';
CREATE DATABASE run247_db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON run247_db.* TO 'run247'@'localhost';
FLUSH PRIVILEGES;