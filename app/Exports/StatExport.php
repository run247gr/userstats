<?php

namespace App\Exports;

use App\Models\Userstat;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Auth;

class StatExport implements FromCollection, WithCustomCsvSettings, WithHeadings
{
    private $user;

    function __construct($user) { $this->user = $user; }

    public function getCsvSettings(): array { return [ 'delimiter' => ',' ]; }

    public function headings(): array { 
        $labels = Userstat::LABELS;
        unset($labels['tracker_type']);
        return array_values($labels);
    }

    public function collection()
    {
        if(! $this->user ) return NULL;
        $userstats = Userstat::where('user_id',$this->user->id )->get()->makeHidden(['id', 'user_id','tracker_type','created_at','updated_at']);
        return $userstats;
    }
}
