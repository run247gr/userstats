<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Userstat extends Model
{
    use HasFactory;

    const LABELS = [
        "activity_date" => "Date",
        "user_id" => "User Id",
        "tracker_type" => "Tracker Type",
        "activity_id" => "Activity Id",
        "activity_type" => "Activity Type",
        "favorite" => "Favorite",
        "name" => "Title",
        "distance" => "Distance",
        "calories" => "Calories",
        "elapsed_time" => "Time",
        "heart_rate_avg" => "Avg HR",
        "heart_rate_max" => "Max HR",
        "aerobic" => "Aerobic TE",
        "candence_avg" => "Avg Run Cadence",
        "candence_max" => "Max Run Cadence",
        "speed_avg" => "Avg Pace",
        "speed_max" => "Best Pace",
        "elevation_gain" => "Total Ascent",
        "elevetion_loss" => "Total Descent",
        "stride_len_avg" => "Avg Stride Length",
        "vertical_ratio_avg" => "Avg Vertical Ratio",
        "vertical_oscil_avg" => "Avg Vertical Oscillation",
        "stress_score" => "Training Stress Score®",
        "grit" => "Grit",
        "flow" => "Flow",
        "time_dive" => "Dive Time",
        "temp_min" => "Min Temp",
        "surface_interval" => "Surface Interval",
        "decompression" => "Decompression",
        "time_lap_best" => "Best Lap Time",
        "laps_count" => "Number of Laps",
        "temp_max" => "Max Temp",
        "time_moving" => "Moving Time",
        "time_elapsed" => "Elapsed Time",
        "elevation_min" => "Min Elevation",
        "elevation_max" => "Max Elevation",
        "activity_desc" => "Activity Description",
        "commute" => "Commute",
        "athlete_weight" => "Athlete Weight",
        "bike_weight" => "Bike Weight",
        "elapsed_speed_avg" => "Avg Elapsed Speed",
    ];

    const FIELDS = [
        'user_id',
        'tracker_type',
        'activity_id',
        'activity_type',
        'activity_date',
        'favorite',
        'name',
        'distance',
        'calories',
        'elapsed_time',
        'heart_rate_avg',
        'heart_rate_max',
        'aerobic',
        'candence_avg',
        'candence_max',
        'speed_avg',
        'speed_max',
        'elevation_gain',
        'elevetion_loss',
        'stride_len_avg',
        'vertical_ratio_avg',
        'vertical_oscil_avg',
        'stress_score',
        'grit',
        'flow',
        'time_dive',
        'temp_min',
        'surface_interval',
        'decompression',
        'time_lap_best',
        'laps_count',
        'temp_max',
        'time_moving',
        'time_elapsed',
        'elevation_min',
        'elevation_max',
        'activity_desc',
        'commute',
        'athlete_weight',
        'bike_weight',
        'elapsed_speed_avg',
    ];

    protected $fillable = self::FIELDS;

    protected $hidden = [ 'id','user_id' ];

    public static function tableLabels()
    {
        $labels = self::LABELS;
        unset($labels['user_id']);
        return $labels;
    }

    public static function chartLabels()
    {
        $labels = self::LABELS;
        unset($labels['user_id']);
        unset($labels['activity_id']);
        unset($labels['activity_type']);
        unset($labels['activity_date']);
        unset($labels['name']);
        unset($labels['favorite']);
        unset($labels['commute']);
        return $labels;
    }

    public static function firstLF()
    {
        return array_keys(self::chartLabels())[0];
    }

    public function fieldChartData($field)
    {   
        if(! in_array($field,array_keys(self::chartLabels()) ) ) return NULL;

        $user_id= Auth::user()->id;
        $data = Userstat::where('user_id',$user_id)->get()->sortBy('activity_date')->pluck($field,'activity_date')->toArray();
        $labels = array_keys($data);
        $label = Userstat::LABELS[$field];

        if( $field == 'tracker_type') {
            $chart_type = 'doughnut';
            $data0 = [0,0];
            foreach( $data as $val ) {
                if( $val == '1' ) { $data0[0]++; } 
                else { $data0[1]++; }
            }
            $data = $data0;
            $labels = ['Garmin','Strava'];

            $data = $data0;
        } else {
            $chart_type = 'line';
        }

        return [ 
            "field"  => $field, 
            "label"  => $label, 
            "data"   => $data,
            "labels" => $labels, 
            "chart_type" => $chart_type,
        ];
    }
}
