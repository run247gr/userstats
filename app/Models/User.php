<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'isadmin',
        'strava_access',
        'strava_refresh',
        'strava_expire',
        'accepted_terms',
        'allowed_data_share',
    ];
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at',
        'created_at',
        'updated_at',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
        'strava_expire' => 'timestamp',
        'isadmin' => 'boolean',
    ];

    public function stravaExpired()
    {
        if( $this->strava_expire != null && strtotime($this->strava_expire) < now() ) { return false; }
        return true;
    }
}
