<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StravaConfig extends Model
{
    use HasFactory;

    protected $table = 'config';
    public $timestamps = false;

    protected $fillable = [
        'client_id',
        'client_secret',
        'access_token',
        'refresh_token',
        'redirect_uri',
    ];
}
