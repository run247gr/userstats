<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use SodiumException;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\SodiumBase64Polyfill;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class ValidateJWT
{
    public function handle(Request $request, Closure $next)
    {
        try {
            sodium_base642bin(env('JWT_SECRET'), SodiumBase64Polyfill::SODIUM_BASE64_VARIANT_ORIGINAL, '' );
        } catch (SodiumException $se) {
            return response([ 'message' => 'internal error 00001'],500);
        }
        $config = Configuration::forSymmetricSigner( new Sha256(), InMemory::base64Encoded(env('JWT_SECRET')) );
     
        try {
            $token = $config->parser()->parse($request->bearerToken());
        } catch (\Exception $e) {
            return response([ 'message' => 'unauthorized 000'],401);
        }

        if( $token->isExpired(now()) ) {
            return response([ 'message' => 'unauthorized 001'],401);
        }

        $request->jwtHeaders = $token->headers();
        // $request->jwtClaims  = $token->claims();

        return $next($request);
    }
}
