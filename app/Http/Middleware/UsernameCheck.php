<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsernameCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $path_parts = explode("/",$request->path()); // var_dump($path_parts);
        if( !isset($path_parts[1]) || Auth::user()->name != $path_parts[1] ) {
            return response(["Unauthorized"],401);
        }

        if( isset($path_parts[2]) && 'profile' == $path_parts[2] ) {
            return response(Auth::user(),200);
        }
        
        return $next($request);
    }
}
