<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Userstat;

class CrudController extends Controller
{
    /**
     * Activity Create
     *
     *  @OA\Post(
     *      tags={"Activity CRUD"},
     *      path="/api/{username}/activities",
     *      description="Activity Create",
     *      security={{"bearerAuth":{}}},
     *      
     *      @OA\Parameter(
     *          name="username",
     *          description="User Name",
     *          in="path",
     *          required=true,
     *          example="user1",
     *          @OA\Schema( type="string" ) 
     *      ),
     * 
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object", 
     *                  example = {
     *                      "tracker_type": 1,
     *                      "activity_id": "ff93569df38c3585d87e10",
     *                      "activity_type": "Running",
     *                      "activity_date": "2020-10-27 07:48:06",
     *                      "favorite": 0,
     *                      "name": "Running",
     *                      "distance": 5.51,
     *                      "calories": 382,
     *                      "elapsed_time": 1945,
     *                      "heart_rate_avg": 153,
     *                  }
     *              )
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Error Message", 
     *          @OA\JsonContent( type="object", example= { "status": "error", "message": "activity_type required" } )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful Create", 
     *          @OA\JsonContent(
     *              type="object",
     *              example = {
     *                  "status": "success",
     *                  "userstat": {
     *                      "tracker_type": 1,
     *                      "activity_id": "ff93569df38c3585d87e10",
     *                      "activity_type": "Running",
     *                      "activity_date": "2020-10-27 07:48:06",
     *                      "favorite": 0,
     *                      "name": "Running",
     *                      "distance": 5.51,
     *                      "calories": 382,
     *                      "elapsed_time": 1945,
     *                      "heart_rate_avg": 153,
     *                  }
     *              }
     *          )
     *      )
     *  )
     */

    public function createActivity(Request $request,$username)
    {
        $user = Auth::user();
        $data = [];
        foreach( Userstat::FIELDS as $field ) {
            if( isset($request->$field) ) {
                $data[$field] = $request->$field;
            }
        } // dd($data);

        if(! isset($data['activity_type']) ) {
            return response(['status' => 'error' , 'message '=> 'activity_type required'],400);
        }
        $data['tracker_type'] = 0;
        if(! isset($data['activity_date']) ) {
            $data['activity_date'] = now();
        }
        $data['user_id'] = $user->id;

        if(! isset($data['activity_id']) ) {
            $sha1_str = $data['user_id'].$data['tracker_type'].$data['activity_type'].$data['activity_date'];
            $data['activity_id'] = sha1($sha1_str);
        }

        $userstat = Userstat::create($data);

        return response(['status' => 'success', 'userstat' => $userstat],200);
    }

    /**
     * Activity Get
     *
     *  @OA\Get(
     *      tags={"Activity CRUD"},
     *      path="/api/{username}/activities/{id}",
     *      description="Activity Get",
     *      security={{"bearerAuth":{}}},
     *       
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema( type="bearerAuth" ) 
     *      ),
     *      @OA\Parameter(
     *           name="username",
     *           description="User Name",
     *           in="path",
     *           required=true,
     *           example="user1",
     *           @OA\Schema( type="string" ) 
     *       ),
     *       @OA\Parameter(
     *           name="id",
     *           description="Activity ID",
     *           in="path",
     *           required=true,
     *           example="ff93569df38c3585d87e10",
     *           @OA\Schema( type="string" ) 
     *       ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Error Message", 
     *          @OA\JsonContent( type="object", example= { "status": "error", "message": "not found" } )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful Get", 
     *          @OA\JsonContent(
     *              type="object",
     *              example = {
     *                  "status": "success",
     *                  "userstat": {
     *                      "tracker_type": 1,
     *                      "activity_id": "ff93569df38c3585d87e10",
     *                      "activity_type": "Running",
     *                      "activity_date": "2020-10-27 07:48:06",
     *                      "favorite": 0,
     *                      "name": "Running",
     *                      "distance": 5.51,
     *                      "calories": 382,
     *                      "elapsed_time": 1945,
     *                      "heart_rate_avg": 153,
     *                  }
     *              }
     *          )
     *      )
     *  )
     */

    public function getActivity(Request $request,$username,$id)
    {
        // if(! $userstat = Userstat::find($id) ) {
        if(! $userstat = Userstat::where('activity_id',$id)->first() ) {
            return response(['status' => 'error', 'message' => 'not found'],400);
        }
        return response(['status' => 'success', 'userstat' => $userstat],200);
    }

    /**
     * Activity Update
     *
     *  @OA\Post(
     *      tags={"Activity CRUD"},
     *      path="/api/{username}/activities/{id}",
     *      description="Activity Update",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema( type="bearerAuth" ) 
     *      ),
     *       @OA\Parameter(
     *           name="username",
     *           description="User Name",
     *           in="path",
     *           required=true,
     *           example="user1",
     *           @OA\Schema( type="string" ) 
     *       ),
     *       @OA\Parameter(
     *           name="id",
     *           description="Activity ID",
     *           in="path",
     *           required=true,
     *           example="ff93569df38c3585d87e10",
     *           @OA\Schema( type="string" ) 
     *       ),
     *
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object", 
     *                  example = {
     *                      "tracker_type": 1,
     *                      "activity_id": "ff93569df38c3585d87e10",
     *                      "activity_type": "Running",
     *                      "activity_date": "2020-10-27 07:48:06",
     *                      "favorite": 0,
     *                      "name": "Running",
     *                      "distance": 5.51,
     *                      "calories": 382,
     *                      "elapsed_time": 1945,
     *                      "heart_rate_avg": 153,
     *                  }
     *              )
     *          )
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Error Message", 
     *          @OA\JsonContent( type="object", example= { "status": "error", "message": "not found" } )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful Update", 
     *          @OA\JsonContent(
     *              type="object",
     *              example = {
     *                  "status": "success",
     *                  "userstat": {
     *                      "tracker_type": 1,
     *                      "activity_id": "ff93569df38c3585d87e10",
     *                      "activity_type": "Running",
     *                      "activity_date": "2020-10-27 07:48:06",
     *                      "favorite": 0,
     *                      "name": "Running",
     *                      "distance": 5.51,
     *                      "calories": 382,
     *                      "elapsed_time": 1945,
     *                      "heart_rate_avg": 153,
     *                  }
     *              }
     *          )
     *      )
     *  )
     */

    public function updateActivity(Request $request,$username,$id)
    {
        if(! $userstat = Userstat::where('activity_id',$id)->first() ) {
            return response(['status' => 'error', 'message' => 'not found'],400);
        }

        $data = [];
        foreach( Userstat::FIELDS as $field ) {
            if( $request->$field ) {
                $data[$field] = $request->$field;
            }
        }
        
        $userstat->update($data);

        return response(['status' => 'success', 'userstat' => $userstat],200);
    }

    /**
     * Activity Delete
     *
     *  @OA\Delete(
     *      tags={"Activity CRUD"},
     *      path="/api/{username}/activities/{id}",
     *      description="Activity Delete",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema( type="bearerAuth" ) 
     *      ),
     *       @OA\Parameter(
     *           name="username",
     *           description="User Name",
     *           in="path",
     *           required=true,
     *           example="user1",
     *           @OA\Schema( type="string" ) 
     *       ),
     *       @OA\Parameter(
     *           name="id",
     *           description="Activity ID",
     *           in="path",
     *           required=true,
     *           example="ff93569df38c3585d87e10",
     *           @OA\Schema( type="string" ) 
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Error Message", 
     *          @OA\JsonContent( type="object", example= { "status": "error", "message": "not found" } )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful Update", 
     *          @OA\JsonContent( type="object", example = { "status": "success", "userstat_id": 153 } )
     *      )
     *  )
     */

    public function deleteActivity(Request $request,$username,$id)
    {
        if(! $userstat = Userstat::where('activity_id',$id)->first() ) {
            return response(['status' => 'error', 'message' => 'not found'],400);
        }
        $userstat->delete();

        return response(['status' => 'success', 'userstat_id' => $userstat->id],200);
    }

    /**
     * Activity Specific Measurement
     *
     *  @OA\Get(
     *      tags={"Activity CRUD"},
     *      path="/api/{username}/{field}",
     *      description="Activity Specific",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         description="Bearer {access-token}",
     *         @OA\Schema( type="bearerAuth" ) 
     *      ),
     *      @OA\Parameter(
     *           name="username",
     *           description="User Name",
     *           in="path",
     *           required=true,
     *           example="user1",
     *           @OA\Schema( type="string" ) 
     *      ),
     *      @OA\Parameter(
     *           name="field",
     *           description="Specific Activity Measurement",
     *           in="path",
     *           required=true,
     *           example="distance",
     *           @OA\Schema( type="string" ) 
     *      ),
     *
     *      @OA\Response(
     *          response=400,
     *          description="Error Message", 
     *          @OA\JsonContent( type="object", example= { "status": "error", "message": "invalid specific" } )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful Get", 
     *          @OA\JsonContent(
     *              type="object",
     *              example = {
     *                  "status": "success",
     *                  "userstats": {
     *                      "2020-01-16 19:23:56": {
     *                          "activity_type": "Running",
     *                          "distance": 12.18
     *                      },
     *                      "2020-01-18 13:54:00": {
     *                          "activity_type": "Indoor Running",
     *                          "distance": 8
     *                      },
     *                      "2020-01-19 14:38:28": {
     *                          "activity_type": "Running",
     *                          "distance": 13.08
     *                      },
     *                      "2020-01-21 18:16:40": {
     *                          "activity_type": "Running",
     *                          "distance": 13.2
     *                      }
     *                  }
     *              }
     *          )
     *      )
     *  )
     */

    public function getActivityField(Request $request,$username,$field)
    {
        $invalid = [ 'user_id','activity_id','activity_date' ];

        if( in_array($field,$invalid) || !in_array($field,Userstat::FIELDS) ) {
            return response(['status' => 'error' , 'message '=> 'invalid specific'],400);
        }

        $user_id = Auth::user()->id;
        $userstats_raw = DB::select(DB::raw("SELECT u.id, u.activity_date, u.activity_type, u.`$field` FROM userstats u WHERE u.user_id = $user_id ORDER BY u.activity_date"));

        $userstats = [];
        foreach($userstats_raw as $row ) {
            $userstats[$row->activity_date] = [
                'activity_type' => $row->activity_type,
                $field => $row->$field,
            ];
        }

        return response(['status' => 'success', 'userstats' => $userstats],200);
    }    
    
}
