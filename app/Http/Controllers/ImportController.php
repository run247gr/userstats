<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\GarminImport;
use App\Imports\StravaImport;
use App\Models\Userstat;
use App\Models\User;
use Carbon\Carbon;

class ImportController extends Controller
{
    public function activities(Request $request)
    {
        $stats = Userstat::where('user_id',Auth::user()->id)->get();
        $labels = Userstat::LABELS; 
        $fields = Userstat::FIELDS;
        unset($fields[array_search('user_id',$fields)]);
        return view('activities',[ 'stats' => $stats, 'labels' => $labels, 'fields' => $fields, ]);
    }

    public function importGarmin(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $request->validate([ 'file_garmin' => ['required'], ],[ 'file_garmin.required' => 'Please choose a file first.', ]);
        Excel::import(new GarminImport($user), $request->file('file_garmin'));
        $time = (new Carbon(now()))->subSeconds(5);
        $newstats = Userstat::where('user_id',$user_id)->where('created_at','>=',$time)->get();
        return back()
            ->with('file_garmin', 'Data Imported Successfully!')
            ->with('stats', $newstats)
        ;
    }

    public function importStrava(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $request->validate( [ 'file_strava' => ['required'], ],[ 'file_strava.required' => 'Please choose a file first.', ]);
        Excel::import(new StravaImport($user), $request->file('file_strava'));
        $time = (new Carbon(now()))->subSeconds(5);
        $newstats = Userstat::where('user_id',$user_id)->where('created_at','>=',$time)->get();
        return back()
            ->with('file_strava', 'Data Imported Successfully!')
            ->with('stats', $newstats)
        ;
    }

    public function clearData(Request $request)
    {
        DB::select(DB::raw('TRUNCATE TABLE userstats'));
        return redirect('/import');
    }

    public function genData(Request $request)
    {
        $request->validate([ 'number' => ['required'], 'user_id' => ['required'], ]);

        $N = $request->number;
        $user_id = $request->user_id;

        $types = ['running','biking','swiming'];
        $date = date('Y-m-d H:i:s');

        for($i=0; $i<$N; $i++) {

            $data = [
                'user_id'        => $user_id,
                'tracker_type'   => rand(1,2),
                'activity_id'    => rand(1000000000,9999999999),
                'activity_type'  => $types[rand(0,2)],
                'activity_date'  => $date,
                'calories'       => rand(500,999),
                'elapsed_time'   => rand(1500,2000),
                'heart_rate_avg' => rand(180,190),
                'heart_rate_max' => rand(180,190),
                'athlete_weight' => rand( 80,100),
            ];
            Userstat::create($data);

            $date = date('Y-m-d H:i:s', strtotime($date .' +2 day'));
        }

        return back()->with('gen_data', "Data Generated Successfully! ($N,$user_id)");
    }
    
    /**
     * Api Import
     *
     *  @OA\Post(
     *      tags={"Import/Export"},
     *      path="/api/{username}/activity?type={type}",
     *      description="Api Import",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *          name="username",
     *          description="User Name",
     *          in = "path",
     *          required=true,
     *          example="user1",
     *          @OA\Schema( type="string" ) 
     *      ),
     *      @OA\Parameter(
     *          name="type",
     *          description="Type",
     *          in = "query",
     *          required=true,
     *          example="csv",
     *          @OA\Schema( type="string" ) 
     *      ),
     * 
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property( property="file", type="file", format="file" )
     *              )
     *          )
     *      ),
     * 
     *      @OA\Response(
     *          response=401,
     *          description="Invalid Credentials", 
     *          @OA\JsonContent( example="Error : Invalid Credentials" )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful upload", 
     *          @OA\JsonContent( type="object", example= { "message": "success" } )
     *      )
     *  )
     */

    public function apiImport(Request $request,$username)
    {
        $request->validate([ 
            'type' => ['required', 'in:garmin,strava'],
            'file' => ['required',],
        ]);

        $user = User::where('name',$username)->first();
        var_dump($user);

        if( $request->type == 'garmin' ) { 
            Excel::import(new GarminImport($user), $request->file('file'));
        } else if( $request->type == 'strava ' ) {
            Excel::import(new StravaImport($user), $request->file('file'));
        }

        return response(['message' => "success"],200);
    }

}
