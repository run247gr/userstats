<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\StravaConfig;

class ConfigController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        $configs = StravaConfig::all();

        if( $configs->count() == 0 ) 
        {
            $data =  [
                'client_id' => '',
                'client_secret' => '',
                'access_token' => '',
                'refresh_token' => '',
                'redirect_uri' => '',
            ];
        } else {
            $data = $configs->first();
        }

        return view('admin_config',$data);
    }

    public function update(Request $request)
    {
        $configs = StravaConfig::all();
        $data = $request->all();

        if( $configs->count() == 0 ) 
        {
            $config = StravaConfig::create($data);
        } 
        else 
        {
            $config = $configs->first();
            $config->update($data);
        }

        return back()->with('config_data', "Configuration Saved Successfully !");

    }
}
