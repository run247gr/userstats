<?php

namespace App\Http\Controllers;

use App\Models\Userstat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

use App\Exports\StatExport;
use App\Models\User;
use SimpleXMLElement;
use DOMDocument;

class ExportController extends Controller
{
    public function jsonExport(Request $request)
    {
        return $this->downloadJson(Auth::user());
    }

    public function csvExport(Request $request)
    {
        return $this->downloadCSV(Auth::user());
    }

    public function xmlExport(Request $request)
    {
        return $this->downloadXML(Auth::user());
    }

    public function webExport(Request $request)
    {
        $request->validate([ 'format' => ['required', 'in:json,xml,csv,rdf']]);

        $user = Auth::user();

        if( $request->format == 'json' ) {
            return $this->downloadJson($user);
        } 
        else if( $request->format == 'csv' ) { 
            return $this->downloadCSV($user);
        }
        else if( $request->format == 'xml' ) { 
            return $this->downloadXML($user);
        } 
        else if( $request->format == 'rdf' ) { 
            return $this->downloadRDF($user);
        } 
    }

    /**
     * Api Export
     *
     *   @OA\Get(
     *       tags={"Import/Export"},
     *       path="/api/{username}/activities?format={format}",
     *       description="Api Export",
     *       security={{"bearerAuth":{}}},
     * 
     *       @OA\Parameter(
     *           name="username",
     *           description="User Name",
     *           in = "path",
     *           required=true,
     *           example="user1",
     *           @OA\Schema( type="string" ) 
     *       ),
     *       @OA\Parameter(
     *           name="format",
     *           description="Format",
     *           in = "query",
     *           required=true,
     *           example="csv",
     *           @OA\Schema( type="string" ) 
     *       ),
     *
     *       @OA\Response(
     *           response=401,
     *           description="Error Message", 
     *           @OA\JsonContent( type="object", example= { "message": "Unauthenticated." } )
     *       ),
     *       @OA\Response(
     *           response=200,
     *           description="Successful Exported", 
     *           @OA\JsonContent(
     *               type="array",
     *               @OA\Items(
     *                   type = "object",
     *                   example = {
     *                       "tracker_type": 1,
     *                        "activity_id": "ff93569df38c3585d87e10",
     *                        "activity_type": "Running",
     *                       "activity_date": "2020-10-27 07:48:06",
     *                       "favorite": 0,
     *                       "name": "Running",
     *                       "distance": 5.51,
     *                       "calories": 382,
     *                       "elapsed_time": 1945,
     *                       "heart_rate_avg": 153,
     *                   }
     *               )
     *           ),
     *           @OA\XmlContent(
     *               @OA\Xml( name="userstats", wrapped=true ),
     *               type="array",
     *               @OA\Items(
     *                   type="array",
     *                   @OA\Xml( name="userstat" ),
     *                   @OA\Items(
     *                       type="object",
     *                       example = {
     *                           "tracker_type": 1,
     *                           "activity_id": "ff93569df38c3585d87e10",
     *                           "activity_type": "Running",
     *                           "activity_date": "2020-10-27 07:48:06",
     *                           "favorite": 0,
     *                           "name": "Running",
     *                           "distance": 5.51,
     *                           "calories": 382,
     *                           "elapsed_time": 1945,
     *                           "heart_rate_avg": 153,
     *                       }
     *                   )
     *                )
     *           ),
     *           @OA\MediaType(
     *               mediaType="text/csv",
     *               example = "'Activity Id','Activity Type','Date','Favorite','Title','Distance','Calories','Time','Avg HR','Max HR','Aerobic TE','Avg Run Cadence'
'ff93569df38c3585d87e10aa142a32f70da0f074','Running','2020-10-27 07:48:06','','Running','5.51','382','1945','153','179','3.6','162'
'45c604d5e9a29736f167e5548a1ff3b3c35fe64c','Stopwatch','2020-08-23 09:34:57','','Stopwatch','5.54','402','973','','179','3.6','162'
'457171925fc6e671c344e9dfba1dc33fe2437fce','Running','2020-08-16 10:12:04','','Marathonas Running','6.08','486','2250','','173','4','169'
'2968c02b977d478697be39548c1d67b3fb05fb2a','Cycling','2020-07-27 18:00:21','','Syvota Cycling','1.33','235','4444','165','174','4','170'
'bc7ddc3ab4030ac12180d93465686a2ffb51fdd5','Cycling','2020-05-02 10:30:44','','Athens Cycling','15.77','639','4050','135','170','2.8','175'
"
     *                          
     *           )
     *       )
     *   )
     */

    public function apiExport(Request $request,$username)
    {
        $request->validate([ 'format' => ['required', 'in:json,xml,csv,rdf']] ,
            [ 'format.required' => 'The format field is required. For example : ?format=json', ]);
        $user = Auth::user();

        if( $request->format == 'json' ) {
            return $this->downloadJson($user);
        } 
        else if( $request->format == 'csv' ) { 
            return $this->downloadCSV($user);
        }
        else if( $request->format == 'xml' ) { 
            return $this->downloadXML($user);
        } 
        else if( $request->format == 'rdf' ) { 
            return $this->downloadRDF($user);
        } 
    }

    private function downloadJson($user)
    {
        $data = Userstat::where('user_id',$user->id)->get();
        $json_file['data'] = json_encode($data,JSON_PRETTY_PRINT);

        $file_name = 'userstats_'.($user->name).'_'.time().'.json';
        File::put(public_path('/json'.'/'.$file_name),$json_file);
        return response()->download(public_path('/json'.'/'.$file_name))->deleteFileAfterSend();
    }

    public function downloadCSV($user)
    {
        $file_name = 'userstats_'.($user->name).'_'.time().'.csv';
        return Excel::download(new StatExport($user), $file_name);
    }

    public function downloadXML($user)
    {
        $data = Userstat::where('user_id',$user->id)->get()->toArray();
        
        $xml = new SimpleXMLElement('<userstats/>');
        foreach( $data as $entry ) {
            $e_xml = $xml->addChild('userstat');
            $e_xml->addAttribute('activity_id',$entry['activity_id']);
            $e_xml->addAttribute('activity_date',$entry['activity_date']);
            foreach( $entry as $field => $val ) {
                if( $val != NULL && $field != 'id' && $field != 'user_id' ) {
                    $e_xml->addChild($field,$val);
                }
            }
        }

        $dom = new DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml->asXML());
        $xml_file['data'] = $dom->saveXML();

        $file_name = 'userstats_'.($user->name).'_'.time().'.xml';
        File::put(public_path('/xml'.'/'.$file_name),$xml_file);
        return response()->download(public_path('/xml'.'/'.$file_name))->deleteFileAfterSend();
    }

    public function downloadRDF($user)
    {
        $data = Userstat::where('user_id',$user->id)->get()->toArray();
       
        $rdf = '<?xml version="1.0"?>';
        $rdf .= '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:userstat="https://stats.run247.gr/activities" >';
        foreach( $data as $entry ) {
            $rdf .= '<rdf:Description rdf:activity_id="'.$entry['activity_id'].'">';
            foreach( $entry as $field => $val ) {
                if( $val != NULL && $field != 'id' && $field != 'user_id' ) {
                    $rdf .= '<userstat:'.$field.'>'.$val.'</userstat:'.$field.'>';
                }
            }
            $rdf .= '</rdf:Description>';
        }
        $rdf .= '</rdf:RDF>';

        $dom = new DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($rdf);
        $xml_file['data'] = $dom->saveXML();

        $file_name = 'userstats_'.($user->name).'_'.time().'.rdf.xml';
        File::put(public_path('/xml'.'/'.$file_name),$xml_file);
        return  response()->download(public_path('/xml'.'/'.$file_name))->deleteFileAfterSend();
    }

}
