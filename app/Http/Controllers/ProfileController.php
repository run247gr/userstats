<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Api Profile
     *
     *  @OA\Get(
     *      tags={"Login/Profile"},
     *      path="/api/{username}/profile",
     *      description="Api Profile",
     *      security={{"bearerAuth":{}}},
     * 
     *       @OA\Parameter(
     *           name="username",
     *           description="User Name",
     *           in = "path",
     *           required=true,
     *           example="user1",
     *           @OA\Schema( type="string" ) 
     *       ),
     * 
     *       @OA\Response(
     *           response=401,
     *           description="Error Message", 
     *           @OA\JsonContent( type="object", example= { "message": "Unauthenticated." }  )
     *       ),
     *       @OA\Response(
     *           response=200,
     *           description="Profile Info", 
     *           @OA\JsonContent( type="object", example= { 
     *              "id": 52, 
     *              "name": "user52", 
     *              "email": "user52@run247userstats.gr",
     *                "accepted_terms": 1,
     *                "allowed_data_share": 1,
     *                "strava_access":  "a9bd14a3db5d435554d3ec0cefb155555502942e",
     *                "strava_refresh": "e81849c4b3e88e555949a1dede4f5555556e2bb5",
     *                "strava_expire": 1667228587,
     *                "isadmin": false
     *          } )
     *       )
     *  )
     */
    public function apiProfile(Request $request,$username)
    {
        // UsernameCheck override
        return response(Auth::user(),200);
    }



     public function editProfileW(Request $request)
     {
        return $this->editProfile($request,Auth::user()->name);
     }

    public function editProfile(Request $request,$username)
    {
        $request->validate([
            'name' => [ 'string', 'max:255'],
            'email' => [ 'string', 'email:rfc', 'max:255', 'unique:users,email,'.Auth::user()->id],
            'password' => [ 'confirmed'],
        ]);

        $fields = $request->all();
        foreach( $fields as $name => $value ) { 
            if( $value == NULL ) { unset($fields[$name]); } 
            else if( $name == 'password' ) {
                $fields[$name] = bcrypt($fields[$name]);
            }
            else if( $name == 'allowed_data_share' ) {
                if( $value == "on" ) {
                    $fields[$name] = 1;
                }
            }
        }

        if(! $request->allowed_data_share ) {
            $fields['allowed_data_share'] = 0;
        }

        $user = User::find(Auth::user()->id);
        $user->update($fields);

        return back()->with('editmess',"Profile updated successfully !");
    }
}
