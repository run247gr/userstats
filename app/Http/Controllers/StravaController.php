<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Userstat;
use App\Models\StravaConfig;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class StravaController extends Controller
{
    const URL = [
        'authorize'  => 'https://www.strava.com/oauth/authorize',
        'token'      => 'https://www.strava.com/oauth/token',
        'activities' => 'https://www.strava.com/api/v3/athlete/activities?page=1',
        'callback'   => 'https://stats.run247.gr/api/strava-callback',
    ];

    const SCOPE = "profile:read_all,profile:write,activity:read,activity:write";
    
    public function getAccessToken(Request $request)
    {
        $config = StravaConfig::all()->first();
        $url = self::URL['authorize']
            ."?client_id=".$config->client_id
            ."&response_type=code"
            ."&redirect_uri=".$config->redirect_uri
            // ."&redirect_uri=".self::URL['callback']
            ."&scope=".self::SCOPE;
        return redirect($url);
    }

    public function stravaCallback(Request $request)
    {
        $config = StravaConfig::all()->first();
        $url = self::URL['token']
            ."?client_id=".$config->client_id
            ."&client_secret=".$config->client_secret
            ."&code=".$request->code
            ."&scope=".$request->scope
            ."&grant_type=authorization_code";
        $res_obj = $this->curlCall($url);

        if( isset($res_obj["message"]) && $res_obj["message"] == "Bad Request" ) {
            return redirect()->route('import')
            ->with('app_strava', 'Authentication Failed')
            ->with('res_obj', $res_obj)
            ;
        }
 
        if( !isset($res_obj["access_token"]) || !isset($res_obj["refresh_token"]) || !isset($res_obj["expires_at"]) ) {
            return redirect()->route('import')
            ->with('app_strava', 'Authentication Failed')
            ->with('res_obj', $res_obj)
            ;
        }
        
        $user = Auth::user();
        $user->update([
            'strava_access'  => $res_obj['access_token'],
            'strava_refresh' => $res_obj['refresh_token'],
            'strava_expire'  => date("Y-m-d H:i:s",$res_obj['expires_at']),
        ]);

        return redirect()->route('import')
        ->with('app_strava', 'Authentication Succeeded!')
        ->with('res_obj', $res_obj)
        ;
    }

    public function importAppStrava(Request $request)
    {
        $token = Auth::user()->strava_access;
        $url = self::URL['activities'];
        $header = [ 'Authorization: Bearer '.$token ] ;
        $res_obj = $this->curlCall($url,'GET',$header);

        if( isset($res_obj["message"]) && ($res_obj["message"] == "Authorization Error" || $res_obj["message"] == "Bad Request") ) {
            return redirect('api/strava-getaccess');
        }

        $new_userstat = $this->insertData($res_obj);

        return redirect()->route('import')
        ->with('ath_strava', 'App Data Imported Successfully!!')
        ->with('res_obj', $res_obj)
        ->with('stats', $new_userstat)
        ;
    }

    private function curlCall($url,$method='POST',$header=[])
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => $header,
        ]);
        $res_raw = curl_exec($curl);
        $res = json_decode($res_raw,true);
        curl_close($curl);
        return $res;
    }

    private function insertData($res_obj)
    {   
        $new_userstat = [];
        foreach( $res_obj as $obj ) 
        {
            if(! $userstat = Userstat::where('activity_id',$obj['id'])->first() ) 
            {
                $userstat = Userstat::create([
                    'user_id' => Auth::user()->id,
                    'tracker_type' => 0,
                    'activity_id' => $obj['id'],
                    'activity_type' => strtolower($obj['type']),
                    'activity_date' => date("Y-m-d H:i:s",strtotime($obj['start_date'])),
                    'favorite' => null,
                    'name' => $obj['name'],
                    'distance' => $obj['distance'],
                    'calories' => null,
                    'elapsed_time' => $obj['distance'],
                    'heart_rate_avg' => null,
                    'heart_rate_max' => null,
                    'aerobic' => null,
                    'candence_avg' => null,
                    'candence_max' => null,
                    'speed_avg' => $obj['average_speed'],
                    'speed_max' => $obj['max_speed'],
                    'elevation_gain' => $obj['total_elevation_gain'],
                    'elevetion_loss' => null,
                    'stride_len_avg' => null,
                    'vertical_ratio_avg' => 0.00,
                    'vertical_oscil_avg' => 0.00,
                    'stress_score' => 0.00,
                    'grit' => 0.00,
                    'flow' => 0.00,
                    'time_dive' => null,
                    'temp_min' => null,
                    'surface_interval' => null,
                    'decompression' => null,
                    'time_lap_best' => null,
                    'laps_count' => null,
                    'temp_max' => null,
                    'time_moving' => null,
                    'time_elapsed' => null,
                    'elevation_min' => $obj['elev_low'],
                    'elevation_max' => $obj['elev_high'],
                    'activity_desc' => null,
                    'commute' => $obj['commute'],
                    'athlete_weight' => null,
                    'bike_weight' => null,
                    'elapsed_speed_avg' => null,
                ]);
                $new_userstat []= $userstat;
            }
        }
        return $new_userstat;
    }

}
