<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Controllers\Controller;

use App\Models\User;

/**
 * @OA\Info(title="Run247 Userstats API", version="0.1", )
 * @OA\SecurityScheme(
 *      securityScheme="bearerAuth",
 *      in="header",
 *      name="bearerAuth",
 *      type="http",
 *      scheme="bearer",
 *      bearerFormat="Custom",
 * ),
 */
class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if( Auth::check() ) { return view('home'); }
        else { return view('myauth.login'); }
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();

        $request->session()->regenerate();

        return redirect('home');
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    /**
     * Api Login
     *
     * @OA\Post(
     *     tags={"Login/Profile"},
     *     path="/api/login",
     *     description="Api Login",
     *
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                      property="email",
     *                      description="email",
     *                      example="user@mailservice.com",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                      property="password",
     *                      description="password",
     *                      example="******",
     *                      type="string"
     *                 )
     *             )
     *         )
     *     ),
     * 
     *     @OA\Response(
     *         response=401,
     *         description="Invalid Credentials",
     *         @OA\JsonContent( type="object", example= { "status": "error", "message": "Error : Invalid Credentials" } )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful created", 
     *         @OA\JsonContent( type="object", example= {  "token": "14|LYUrABWIPRmsxZdjMUxXVJQZO8mGGBq7DsCMbvEN", "username": "user1" } )
     *     ),
     * )
     */
    
    public function createApi(Request $request)
    {
        $request->validate([ 'email' => ['required'], 'password' => ['required'], ]);

        $user = User::where('email', $request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
           return response('Error : Invalid Credentials', 401);
        }

        $user->tokens()->delete();
        return [
            'token'    => $user->createToken('default',['server:update'])->plainTextToken,
            'username' => $user->name,
        ];
    }
}
