<?php

namespace App\Imports;

use Carbon\Carbon;

use App\Models\Userstat;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Illuminate\Support\Facades\Auth;

class GarminImport implements ToModel, WithStartRow, WithCustomCsvSettings
{
    private $user;

    function __construct($user) { $this->user = $user; }

    public function startRow(): int { return 2; }

    public function getCsvSettings(): array { return [ 'delimiter' => ',' ]; }

    public function model(array $row)
    {
        if(! $this->user ) return NULL;
        $data = [];
        $data['user_id'] = $this->user->id;
        $data['tracker_type'] = 1;
        $sha1_str = $data['user_id']. $data['tracker_type'].$row[ 0].$row[ 1];
        $data['activity_id']  = sha1($sha1_str);
        if( Userstat::where('user_id',$data['user_id'])->where('activity_id',$data['activity_id'])->first()  ) { return NULL; }

        if( $row[ 0] != '--' ) $data['activity_type']  = $row[0];                           //  0
        if( $row[ 1] != '--' ) $data['activity_date']  = new Carbon($row[1]);               //  1
        if( $row[ 2] != '--' ) $data['favorite']       = ($row[2] == 'true');               //  2
        if( $row[ 3] != '--' ) $data['name']           = $row[3];                           //  3
        if( $row[ 4] != '--' ) $data['distance']       = floatval($row[4]);                 //  4
        if( $row[ 5] != '--' ) $data['calories']       = intval($row[5]);                   //  5
        if( $row[ 6] != '--' ) $data['elapsed_time']   = $this->strtosec($row[6]);          //  6
        if( $row[ 7] != '--' ) $data['heart_rate_avg'] = intval( $row[ 7] );                //  7
        if( $row[ 8] != '--' ) $data['heart_rate_max'] = intval( $row[ 8] );                //  8
        if( $row[ 9] != '--' ) $data['aerobic']        = floatval($row[9] );                //  9
        if( $row[10] != '--' ) $data['candence_avg']   = intval( $row[10] );                // 10
        if( $row[11] != '--' ) $data['candence_max']   = intval( $row[11] );                // 11
        if( $row[12] != '--' ) $data['speed_avg']      = $this->strtosec( $row[12] );       // 12
        if( $row[13] != '--' ) $data['speed_max']      = $this->strtosec( $row[13] );       // 13
        if( $row[14] != '--' ) $data['elevation_gain'] = intval( $row[14] );                // 14
        if( $row[15] != '--' ) $data['elevetion_loss'] = intval( $row[15] );                // 15
        if( $row[16] != '--' ) $data['stride_len_avg'] = floatval( $row[16] );              // 16

        if( $row[17] != '--' ) $data['vertical_ratio_avg'] = 0.1+floatval( $row[17] );      // 17
        if( $row[18] != '--' ) $data['vertical_oscil_avg'] = 0.1+floatval( $row[18] );      // 18
        if( $row[19] != '--' ) $data['stress_score']       = 0.1+floatval( $row[19] );      // 19
        if( $row[20] != '--' ) $data['grit']               = 0.1+floatval( $row[20] );      // 20
        if( $row[21] != '--' ) $data['flow']               = 0.1+floatval( $row[21] );      // 21

        if( $row[22] != '--' ) $data['time_dive']      = $this->strtosec( $row[22] );       // 22
        if( $row[23] != '--' ) $data['temp_min']       = intval( $row[23] );                // 23
        if( $row[24] != '--' ) $data['surface_interval'] = $this->strtosec( $row[24]);      // 24
        if( $row[25] != '--' ) $data['decompression']  = ($row[25] == 'YES');               // 25

        if( $row[26] != '--' ) $data['time_lap_best']  = $this->strtosec( $row[26]);        // 26
        if( $row[27] != '--' ) $data['laps_count']     = intval( $row[27] );                // 27
        if( $row[28] != '--' ) $data['temp_max']       = intval( $row[28] );                // 28

        if( $row[29] != '--' ) $data['time_moving']    = $this->strtosec( $row[29] );       // 29
        if( $row[30] != '--' ) $data['time_elapsed']   = $this->strtosec( $row[30] );       // 30

        if( $row[31] != '--' ) $data['elevation_min']  = intval( $row[31] );                // 31
        if( $row[32] != '--' ) $data['elevation_max']  = intval( $row[32] );                // 32

        return new Userstat($data);
    }

    private function strtosec($str)
    {
        $seconds = NULL;

        $time_parts = explode(':',$str);
        if( count($time_parts) == 3 ) {
            $seconds = intval($time_parts[0])*60*60+ intval($time_parts[1])*60+ intval($time_parts[2]);
        }
        if( count($time_parts) == 2 ) {
            $seconds = intval($time_parts[0])*60+ intval($time_parts[1]);
        }

        return $seconds;
    }
}
