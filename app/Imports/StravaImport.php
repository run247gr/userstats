<?php

namespace App\Imports;

use Carbon\Carbon;

use App\Models\Userstat;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Illuminate\Support\Facades\Auth;

class StravaImport implements ToModel, WithStartRow, WithCustomCsvSettings
{
    private $user;

    function __construct($user) { $this->user = $user; }

    public function startRow(): int { return 2; }

    public function getCsvSettings(): array { return [ 'delimiter' => ',' ]; }

    public function model(array $row)
    {
        if(! $this->user ) return NULL;

        $data = [];
        $data['user_id'] = $this->user->id;
        $data['tracker_type'] = 2;
        if( Userstat::where('user_id',$data['user_id'])->where('activity_id',$row[ 0])->first()  ) { return NULL; }
        if( $row[ 0] ) $data['activity_id']     = $row[ 0]; // phone number ?? 10 digits , starting with 69... | see activities.csv
        if( $row[ 1] ) $data['activity_date']   = new Carbon($row[1]);
        if( $row[ 2] ) $data['name']            = $row[ 2];
        if( $row[ 3] ) $data['activity_type']   = $row[ 3];
        if( $row[ 4] ) $data['activity_desc']   = $row[ 4];
        if( $row[ 5] ) $data['elapsed_time']    = $row[ 5];
        if( $row[ 6] ) $data['distance']        = $row[ 6];
        if( $row[ 7] ) $data['heart_rate_max']  = $row[ 7];
        if( $row[ 8] ) $data['heart_rate_avg']  = $row[ 8];
        if( $row[ 9] ) $data['commute']         = $row[ 9];
        // if( $row[10] ) $data['activity_gear']   = $row[10]; 
        // if( $row[11] ) $data['filename']        = $row[11]; // unknown
        if( $row[12] ) $data['athlete_weight']  = $row[12];
        if( $row[13] ) $data['bike_weight']     = $row[13];
        if( $row[14] ) $data['time_elapsed']    = $row[14];
        if( $row[15] ) $data['time_moving']     = $row[15];
        if( $row[16] ) $data['distance']        = $row[16];
        if( $row[17] ) $data['speed_max']       = intval($row[17]);
        if( $row[18] ) $data['speed_avg']       = intval($row[18]);
        if( $row[19] ) $data['elevation_gain']  = intval($row[19]);
        if( $row[20] ) $data['elevetion_loss']  = intval($row[20]);
        if( $row[21] ) $data['elevation_min']   = $row[21];
        if( $row[22] ) $data['elevation_max']   = $row[22];
        // if( $row[23] ) $data['grade_max']       = intval($row[23]);
        // if( $row[24] ) $data['grade_avg']       = intval($row[24]);
        // if( $row[25] ) $data['grade_positive_avg']  = $row[25];
        // if( $row[26] ) $data['grade_negative_avg']  = $row[26];
        if( $row[27] ) $data['candence_max']    = $row[27];
        if( $row[28] ) $data['candence_avg']    = $row[28];
        // if( $row[29] ) $data['heart_rate_max']  = $row[29]; // SOME NAME WITH 7
        // if( $row[30] ) $data['heart_rate_avg']  = $row[30]; // SOME NAME WITH 8
        // if( $row[31] ) $data['watts_max']       = $row[31];
        // if( $row[32] ) $data['watts_avg']       = $row[32];
        if( $row[33] ) $data['calories']        = $row[33];
        if( $row[34] ) $data['temp_max']        = $row[34];
        // if( $row[35] ) $data['temp_avg']        = $row[35];
        // if( $row[36] ) $data['relative_effort'] = $row[36];
        // if( $row[37] ) $data['total_work']      = $row[37]; // unknown
        if( $row[38] ) $data['laps_count']      = $row[38];
        // if( $row[39] ) $data['time_uphill']     = $row[39];
        // if( $row[40] ) $data['time_downhill']   = $row[40];
        // if( $row[41] ) $data['time_other']      = $row[41];
        // if( $row[42] ) $data['perceived_exertion'] = $row[42];
        // if( $row[43] ) $data['type']            = $row[43];
        // if( $row[44] ) $data['time_start']      = $row[44];
        // if( $row[45] ) $data['weigthed_power_avg'] = $row[45];
        // if( $row[46] ) $data['power_count']     = $row[46];
        // if( $row[47] ) $data['preceived_exertion_prefer'] = $row[47];
        // if( $row[48] ) $data['perceived_relative_effort'] = $row[48];
        // if( $row[49] ) $data['commute']         = $row[49]; // SOME NAME WITH 9
        // if( $row[50] ) $data['total_weight_lifted'] = $row[50];
        // if( $row[51] ) $data['from_upload']     = $row[51];
        // if( $row[52] ) $data['grade_adjusted_distance'] = $row[52];
        // if( $row[53] ) $data['weather_observation_time'] = $row[53];
        // if( $row[54] ) $data['weather_visibility'] = $row[54];
        // if( $row[55] ) $data['weather_temp']    = $row[55];
        // if( $row[56] ) $data['apparent_temp']   = $row[56];
        // if( $row[57] ) $data['dewpoint']        = $row[57];
        // if( $row[58] ) $data['humidity']        = $row[58];
        // if( $row[59] ) $data['weather_pressure'] = $row[59];
        // if( $row[60] ) $data['wind_speed']      = $row[60];
        // if( $row[61] ) $data['wind_gust']       = $row[61];
        // if( $row[62] ) $data['wind_bearing']    = $row[62];
        // if( $row[63] ) $data['precipitation_intensity']    = $row[63];
        // if( $row[64] ) $data['time_sunrise']    = $row[64];
        // if( $row[65] ) $data['time_sunset']     = $row[65];
        // if( $row[66] ) $data['moon_phase']      = $row[66];
        // if( $row[67] ) $data['bike']            = $row[67];
        // if( $row[68] ) $data['gear']            = $row[68];
        // if( $row[69] ) $data['precipitation_probability'] = $row[69];
        // if( $row[70] ) $data['precipication_type'] = $row[70];
        // if( $row[71] ) $data['cloud_cover']     = $row[71];
        // if( $row[72] ) $data['weather_visibility'] = $row[72];
        // if( $row[73] ) $data['uv_index']        = $row[73];
        // if( $row[74] ) $data['weather_ozone']   = $row[74];
        // if( $row[75] ) $data['jump_count']      = $row[75];
        // if( $row[76] ) $data['total_grid']      = $row[76];
        // if( $row[77] ) $data['flow_avg']        = $row[77];
        // if( $row[78] ) $data['flagged']         = $row[78];
        if( $row[79] ) $data['elapsed_speed_avg'] = $row[79];
        // if( $row[80] ) $data['dirt_distance']   = $row[80];
        // if( $row[81] ) $data['newly_explored_distance'] = $row[81];
        // if( $row[82] ) $data['newly_explored_distance_dirt'] = $row[82];

        return new Userstat($data);
    }
}
