<?php

namespace App\Imports;

use App\Models\Userstat;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class TestImport implements ToModel, WithStartRow, WithCustomCsvSettings
{
    public function startRow(): int { return 2; }

    public function getCsvSettings(): array { return [ 'delimiter' => ',' ]; }

    public function model(array $row)
    {
        $data = [];
        if( Userstat::where('activity_id',$row[ 1])->first()  ) { return NULL; }
        $data['tracker_type']   = $row[ 0];
        $data['activity_id']    = $row[ 1];
        $data['activity_type']  = $row[ 2];
        $data['activity_date']  = $row[ 3];
        $data['calories']       = $row[ 4];
        $data['elapsed_time']   = $row[ 5];
        $data['heart_rate_avg'] = $row[ 6];
        $data['heart_rate_max'] = $row[ 7];
        $data['athlete_weight'] = $row[ 8];
        return new Userstat($data);
    }
}
