<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CrudController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\StravaController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;


Route::post('login',[AuthenticatedSessionController::class , 'createApi' ] );

Route::middleware(['auth:sanctum','usercheck'])->group(function () {
    
    Route::get( '{username}/activities', [ ExportController::class , 'apiExport'     ] );
    Route::get( '{username}/{field}',    [ CrudController::class, 'getActivityField' ] );
    
    Route::post(  '{username}/activities',      [ CrudController::class, 'createActivity' ] );
    Route::get(   '{username}/activities/{id}', [ CrudController::class, 'getActivity'    ] );
    Route::post(  '{username}/activities/{id}', [ CrudController::class, 'updateActivity' ] );
    Route::delete('{username}/activities/{id}', [ CrudController::class, 'deleteActivity' ] );
    
    Route::get( '{username}/profile',  [ ProfileController::class, 'apiProfile' ] );
    Route::post('{username}/profile',  [ ProfileController::class, 'editProfile' ] );
    Route::post('{username}/activity', [ ImportController::class,  'apiImport'   ] );
    
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get( 'user', function(Request $request) { return $request->user();    } );
    Route::get( 'testexport',       [ ExportController::class , 'testExport'     ] );
});

