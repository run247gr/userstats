<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\StravaController;

require __DIR__.'/auth.php';
// Route::get('dashboard'     ,function () { return view('dashboard');   })->name('dashboard');

Route::middleware(['auth'])->group(function () {

    /** General *********************************************************************************** */
    Route::get( '/'             ,function () { return redirect('login');   });
    Route::get( 'home'          ,function () { return view('home');        });
    Route::get( 'profile'       ,function () { return view('profile');     });
    Route::post('edit-profile'  ,[ProfileController::class,'editProfileW']  );
    Route::get( 'import'        ,function () { return view('import');      })->name("import");
    Route::get( 'activities'    ,function () { return view('activities');  });
    Route::post('garmin-import' ,[ImportController::class, 'importGarmin']  );
    Route::post('strava-import' ,[ImportController::class, 'importStrava']  );
    Route::post('strapp-import' ,[StravaController::class, 'importAppStrava']);
    Route::get( 'chart'         ,function () { return view('chart');       });
    Route::get( 'export'        ,function () { return view('export');      });
    Route::get( 'export-csv'    ,[ExportController::class, 'csvExport']     );
    Route::post('export-json'   ,[ExportController::class, 'jsonExport']    );
    Route::post('export-xml'    ,[ExportController::class, 'xmlExport']     );
    Route::post('export-data'   ,[ExportController::class, 'webExport']     );
    Route::get( 'howto'         ,function () { return view('howto');       });
    
    Route::get( 'sync-strava'   ,function () { return view('syncstrava');  });

    
    Route::get( 'api/strava-getaccess', [ StravaController::class , 'getAccessToken' ] );
    Route::get( 'api/strava-callback',  [ StravaController::class , 'stravaCallback' ] );
    /** Admin Only ******************************************************************************** */
    Route::get( 'ttest'         ,function () { return view('ttest');       })->middleware('admin');
    Route::get( 'gencsv'        ,function () { return view('gencsv');      })->middleware('admin');
    Route::post('generate-data' ,[ImportController::class, 'genData']       )->middleware('admin');
    Route::get( 'clear-data'    ,function () { return view('clear_data');  })->middleware('admin');

    Route::get( 'admin/config'  ,[ConfigController::class, 'index']  )->middleware('admin');
    Route::post('admin/config'  ,[ConfigController::class, 'update'] )->middleware('admin');
    
});
Route::get( 'terms'         ,function () { return view('terms');       });

