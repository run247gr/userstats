<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'superadmin',
            'email' => 'info@run247userstats.gr',
            'password' => bcrypt('demopass1'),
        ]);


        User::create([
            'name' => 'user1',
            'email' => 'user1@run247userstats.gr',
            'password' => bcrypt('demopass1'),
        ]);

        User::create([
            'name' => 'user2',
            'email' => 'user2@run247userstats.gr',
            'password' => bcrypt('demopass1'),
        ]);
    }
}
