<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserAddFieldsStravaTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('strava_expire')->after('remember_token')->nullable();
            $table->string('strava_refresh')->after('remember_token')->nullable();
            $table->string('strava_access')->after('remember_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('strava_access');
            $table->dropColumn('strava_refresh');
            $table->dropColumn('strava_expire');
        });
    }
}
