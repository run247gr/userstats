<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserstatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userstats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->integer('tracker_type')->nullable();
            $table->string('activity_id')->nullable();
            $table->string('activity_type')->nullable();
            $table->timestamp('activity_date')->nullable();
            $table->boolean('favorite')->nullable();
            $table->string('name')->nullable();
            $table->float('distance')->nullable();
            $table->integer('calories')->nullable();
            $table->integer('elapsed_time')->nullable();
            $table->integer('heart_rate_avg')->nullable();
            $table->integer('heart_rate_max')->nullable();
            $table->float('aerobic')->nullable();
            $table->integer('candence_avg')->nullable();
            $table->integer('candence_max')->nullable();
            $table->integer('speed_avg')->nullable();
            $table->integer('speed_max')->nullable();
            $table->integer('elevation_gain')->nullable();
            $table->integer('elevetion_loss')->nullable();
            $table->float('stride_len_avg')->nullable();

            $table->float('vertical_ratio_avg')->default(0.0);
            $table->float('vertical_oscil_avg')->default(0.0);
            $table->float('stress_score')->default(0.0);
            $table->float('grit')->default(0.0);
            $table->float('flow')->default(0.0);

            $table->integer('time_dive')->nullable();
            $table->integer('temp_min')->nullable();
            $table->integer('surface_interval')->nullable();
            $table->boolean('decompression')->nullable();
            $table->integer('time_lap_best')->nullable();
            $table->integer('laps_count')->nullable();
            $table->integer('temp_max')->nullable();
            $table->integer('time_moving')->nullable();
            $table->integer('time_elapsed')->nullable();
            $table->float('elevation_min')->nullable();
            $table->float('elevation_max')->nullable();
            $table->string('activity_desc')->nullable();
            $table->boolean('commute')->nullable();
            $table->float('athlete_weight')->nullable();
            $table->float('bike_weight')->nullable();
            $table->float('elapsed_speed_avg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userstats');
    }
}
