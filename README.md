## About Profibit

Profibit aims to fill the existing gap in the market of collecting and managing personal data by athletic portals/apps and services implementing the intermediate software layer as well as the connectors/mappers that are needed to satisfy the requirements imposed by the EU General DataProtection Regulation and, in particular, Article 20 “Right to data portability”.Currently, individuals that provide personal data to services, websites or even applications (i.e., data controllers) are not able to obtain such data in an easy and structured way or re-use it across different platforms.Profibitwill develop, test and validate a semantic interoperability framework along with a set of tools that will support the right to data portability allowing users to control, transfer or even sell their data by monetising and get out economic profit from it.


## License

Profibit is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
